#!/bin/sh
# Copyright (C) 2023 Denis 'GNUtoo' Carikli
# SPDX-License-Identifier: GPL-3.0-or-later
# See /usr/include/linux/input-event-codes.h for the key <-> values
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 15 # TAB
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 46 # C
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 24 # O
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 49 # N
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 31 # S
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 24 # O
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 38 # L
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 18 # E
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 13 # =
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 20 # T
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 20 # T
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 21 # Y
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 58 # CAPSLOCK
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 31 # S
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 11 # 0
virsh -c qemu:///system send-key gnutoo-trisquel-netinstall 28 # ENTER
