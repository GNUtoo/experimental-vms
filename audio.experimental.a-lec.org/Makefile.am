# Copyright (C) 2023 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This file is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# You should have received a copy of the GNU General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.

dist_pkgdata_DATA = mumble-vm.img

CLEANFILES = \
	first-boot.sh \
	guix-commit.txt \
	index.html \
	mumble-vm.img \
	mumble-vm-machine.scm \
	mumble-vm-system.scm \
	wireguard-post-up.sh

# We want to only update the image when guix commit changes. The trick
# to make that work is to only create or update a file when the revision
# changes.
guix-commit.txt: Makefile
	if [ ! -f $@ ] ; then \
		guix describe | grep '^    commit:' | awk '{print $$2}' > $@ ; \
	elif [ "$(cat $@)" != \
	       "$(guix describe | \
			grep '^    commit:' | awk '{print $$2}')" ] ; then \
		guix describe | grep '^    commit:' | awk '{print $$2}' > $@ ; \
	fi

%.html: %.html.tmpl guix-commit.txt Makefile
	sed 's#DOMAIN#$(DOMAIN)#g' $< > $@

%.scm: %.scm.tmpl guix-commit.txt id_ed25519.pub Makefile signing-key.pub
	sed \
		"s#DOMAIN#$(DOMAIN)#g ; \
		s#ENABLE_WIREGUARD#$(ENABLE_WIREGUARD)#g ; \
		s#LETSENCRYPT_EMAIL#$(LETSENCRYPT_EMAIL)#g ; \
		s#VM_IPV4_ADDRESS#$(VM_IPV4_ADDRESS)#g ; \
		s#VM_IPV6_ADDRESS#$(VM_IPV6_ADDRESS)#g ; \
		s#VM_IPV4_GATEWAY#$(VM_IPV4_GATEWAY)#g ; \
		s#VM_IPV6_GATEWAY#$(VM_IPV6_GATEWAY)#g ; \
		s#VM_IPV4_DNS#$(VM_IPV4_DNS)#g ; \
		s#VM_IPV6_DNS#$(VM_IPV6_DNS)#g ; \
		s#VM_SSH_PUB_KEY#$(VM_SSH_PUB_KEY)#g ; \
		s#VM_SSH_ADDRESS#$(VM_SSH_ADDRESS)#g" $< > $@

%.sh: %.sh.tmpl guix-commit.txt Makefile
	sed \
		"s#DOMAIN#$(DOMAIN)#g ; \
		s#LETSENCRYPT_EMAIL#$(LETSENCRYPT_EMAIL)#g ; \
		s#VM_SSH_ADDRESS#$(VM_SSH_ADDRESS)#g" $< > $@
	chmod +x $@

# Generate default key. Can be changed by replacing id_ed25519.pub.
id_ed25519.pub:
	printf "ssh-ed25519 %s %s" \
	'AAAAC3NzaC1lZDI1NTE5AAAAIH2feuEj4asx0ImCG+cuiPv2WdKF6vMI+cJtZyG9cwUQ' \
	'gnutoo@primary_laptop' \
	> $@

# Generate default key. Can be changed by replacing signing-key.pub.
signing-key.pub:
	printf '(public-key (ecc (curve Ed25519) (q #%s#)))\n' \
	'3A7E1F41E2D5784CFCABB39CB73F99E727D4A5C1ECA79D873587D63D093CC4B5' \
	>$@

TARBALL_SOURCE = \
	first-boot.sh \
	guix-commit.txt \
	index.html \
	mumble-vm-machine.scm \
	mumble-vm-system.scm \
	id_ed25519.pub \
	Makefile \
	signing-key.pub \
	wireguard-post-up.sh

IMAGE_SOURCE = \
	$(TARBALL_SOURCE) \
	mumble-vm.tar.xz

mumble-vm.img: $(IMAGE_SOURCE)
	install \
		`guix system image \
			--image-type=mbr-raw \
			--image-size=6G mumble-vm-system.scm` \
		$@
build: $(IMAGE_SOURCE)
	guix system build \
		--image-type=mbr-raw \
		--image-size=6G mumble-vm-system.scm

mumble-vm.tar: $(TARBALL_SOURCE)
	tar --exclude "id_ed25519" -cf $@ $(TARBALL_SOURCE)

mumble-vm.tar.xz: mumble-vm.tar
	xz -f -9e --verbose $<

mumble-vm.tar.xz.b64: mumble-vm.tar.xz
	base64 $< > $@

deploy: $(IMAGE_SOURCE)
	guix deploy -L . mumble-vm-machine.scm
