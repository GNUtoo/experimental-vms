#!/bin/sh
#
# Copyright (C) 2023 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This file is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# You should have received a copy of the GNU General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
set -e

report()
{
    ret=$?
    message="$@"

    if [ ${ret} -eq 0 ] ; then
	echo "[ OK ] ${message}"
    else
	echo "[ !! ] ${message} failed"
	exit ${ret}
    fi
}

echo ';;L;*' | sfdisk -f /dev/vda  ; report "Resizing /dev/vda1 partition" ; \
partprobe
resize2fs /dev/vda1 ; report "Growing /dev/vda1 filesystem"
