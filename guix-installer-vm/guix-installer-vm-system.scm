;; Copyright (C) 2023 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
;;
;; This file is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or (at
;; your option) any later version.
;;
;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-installer-vm-system)
  #:use-module (gnu)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages disk)
  #:use-module (gnu packages dns)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages screen)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages tls)
  #:use-module (gnu services admin)
  #:use-module (gnu services certbot)
  #:use-module (gnu services ssh)
  #:use-module (gnu services telephony)
  #:use-module (gnu services vpn)
  #:use-module (gnu services web)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:export (guix-installer-vm-operating-system))

(define guix-installer-vm-config
  (package
    (name "guix-installer-vm-config")
    (version "0.1")
    ;; TODO: Make that tarball reproducible
    (source (local-file "guix-installer-vm.tar.xz"))
    (build-system copy-build-system)
    (arguments
     (list
     #:install-plan
     #~(list
        '("first-boot.sh" "share/guix-installer-vm/configs/")
        '("guix-commit.txt" "share/guix-installer-vm/configs/")
        '("guix-installer-vm-system.scm" "share/guix-installer-vm/configs/")
        '("id_ed25519.pub" "share/guix-installer-vm/configs/")
        '("Makefile" "share/guix-installer-vm/configs/")
        '("signing-key.pub" "share/guix-installer-vm/configs/")
        '(#$source
          "share/guix-installer-vm/configs/guix-installer-vm.tar.xz"))))
    (synopsis "Full machine configuration.")
    (description
     "This contains all the configuration files of this machine. This is
needed for unattended upgrades to work.")
    (home-page "DOMAIN")
    (license license:gpl3+)))

(define first-boot-script
  (package
    (name "first-boot-script")
    (version "0.1")
    (source (local-file "first-boot.sh" ))
    (build-system gnu-build-system)
    (arguments
     (list #:tests? #f ;no tests
           #:phases
           #~(modify-phases
              %standard-phases
              (delete 'build)
              (delete 'configure)
              (replace 'install
                       (lambda _
                         (chmod "first-boot.sh" #o755)
                         (install-file
                          "first-boot.sh"
                          (string-append (string-append #$output "/bin"))))))))
    (inputs (list e2fsprogs parted util-linux))
    (synopsis "Script to run on first boot.")
    (description
     "The first-boot.sh script resize the rootfs and updates the system.")
    (home-page #f)
    (license license:gpl3+)))

(define guix-installer-vm-operating-system
  (operating-system
   (bootloader (bootloader-configuration
                (bootloader grub-minimal-bootloader)
                (targets '("/dev/vda"))
                (terminal-outputs '(serial_0))))
   (kernel-arguments (append '("console=ttyS0")))
   (file-systems (cons (file-system
                        (device (file-system-label "Guix_image"))
                        (mount-point "/")
                        (type "ext4")) %base-file-systems))
   (host-name "guix-installer-vm")
   (timezone "Europe/Paris")
   (packages (append (list first-boot-script
                           guix-installer-vm-config
                           htop
                           net-tools
                           nss-certs
                           parted
                           screen)
                     %base-packages))
   (services
    (append
     (list
      ;; Agetty

      ;; ttyS0 is already setup automatically due to the console=ttyS0
      ;; kernel argument
      (service agetty-service-type
               (agetty-configuration (term "xterm-256color")
                                     (tty "ttyS1")))
      (service agetty-service-type
               (agetty-configuration (term "xterm-256color")
                                     (tty "ttyS2")))
      (service agetty-service-type
               (agetty-configuration (term "xterm-256color")
                                     (tty "ttyS3")))
      ;; Networking
      (service
       static-networking-service-type
       (list
        (static-networking
         (addresses (list (network-address
                           (device "eth0")
                           (value "192.168.1.118/16"))
                          (network-address
                           (device "eth0")
                           (value "2001:910:1021::118/64"))))
         (routes (list (network-route
                        (destination "default")
                        (gateway "192.168.0.1"))
                       (network-route
                        (destination "default")
                        (gateway "2001:910:1021::1"))))
         (name-servers (list "192.168.0.1" "2001:910:1021::1")))))
      ;; OpenSSH
      (service openssh-service-type
               (openssh-configuration
                (openssh openssh-sans-x)
                (use-pam? #f)
                (port-number 222)
                (permit-root-login #t)
                (password-authentication? #f)
                (challenge-response-authentication? #f)
                (authorized-keys
                 `(("root" , (local-file "id_ed25519.pub"))
                   ("gnutoo" ,(local-file "id_ed25519.pub"))))))
      ;; Unattended Upgrades
      (service
       unattended-upgrade-service-type
       (unattended-upgrade-configuration
        (operating-system-file
         (string-append "/run/current-system/profile"
                        "/share/guix-installer-vm/configs/"
                        "guix-installer-vm-system.scm"))
        (schedule "0 * * * * ")
        (services-to-restart (list 'guix-daemon 'mcron 'ssh-daemon)))))
     (modify-services
      %base-services
      (guix-service-type config => (guix-configuration
                                    (authorized-keys
                                     (append
                                      (list
                                       (local-file
                                        "signing-key.pub"))
                                      %default-authorized-guix-keys)))))))))
guix-installer-vm-operating-system
