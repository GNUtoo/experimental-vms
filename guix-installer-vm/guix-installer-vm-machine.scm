;;; Copyright © Guix documentation authors
;;; Copyright © 2023 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
;;;
;;; This file is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This file is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (gnu)
             (gnu machine)
             (gnu machine ssh))
(list
 (machine
  (operating-system
   (@ (guix-installer-vm-system) guix-installer-vm-operating-system))
  (environment managed-host-environment-type)
  (configuration
   (machine-ssh-configuration
    (authorize? #t)
    (build-locally? #f)
    (host-key
     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJGgswfvxxErFStqBGY81N0uuLndjA5l9bGd4DGlcK9F")
    (host-name "2001:910:1021::118")
    (identity "./id_ed25519")
    (port 222)
    (system "x86_64-linux")
    (user "root")))))
