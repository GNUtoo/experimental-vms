#!/usr/bin/env bash
#
# Copyright (C) 2024 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

keymaps="/run/current-system/profile/share/keymaps/i386"

ask_keymap_type()
{
    index=0
    keymap_type_list=""
    for keymap_type in "${keymaps}"/* ; do
        # shellcheck disable=SC2001 # For ^ or $ regex.
        keymap_type="$(echo "${keymap_type}" | sed "s#^${keymaps}/##")"
        if [ "${keymap_type}" = "include" ] ; then
            continue
        fi
        keymap_type_list="${keymap_type_list} ${index} ${keymap_type}"
        index=$((index + 1))
    done

    IFS=' ' read -r -a keymap_type_list_array <<< "${keymap_type_list}"

    # shellcheck disable=SC2086
    result=$(dialog --stdout \
                    --menu "Keyboard layout type:" \
                    0 0 0 \
                    ${keymap_type_list})

    if [ "${result}" = "" ] ; then
        exit 0
    fi

    result=$((result * 2))
    result=$((result + 1))

    directory=${keymap_type_list_array[${result}]}
    echo "${directory}"
}

ask_keymap()
{
    directory="$1"

    index=0
    keymap_list=""
    for keymap in "${keymaps}"/"${directory}"/* ; do
        # shellcheck disable=SC2001 # For ^ or $ regex.
        mapname=$(echo "${keymap}" | \
                      sed "s#^${keymaps}/${directory}/##" | \
                      sed 's#\.map\.gz$##')
        keymap_list="${keymap_list} ${index} ${mapname}"
        index=$((index + 1))
    done

    IFS=' ' read -r -a keymap_list_array <<< "${keymap_list}"

    # shellcheck disable=SC2086
    result=$(dialog --stdout --menu "Keyboard layout:" 0 0 0 ${keymap_list})
    if [ "${result}" = "" ] ; then
        exit 0
    fi
    result=$((result * 2))
    result=$((result + 1))

    keymap_name=${keymap_list_array[${result}]}

    echo "${keymaps}/${directory}/${keymap_name}.map.gz"
}

directory="$(ask_keymap_type)"
if [ -z "${directory}" ] ; then
    exit 0
fi

keymap_path="$(ask_keymap "${directory}")"
if [ -z "${keymap_path}" ] ; then
    exit 0
fi

loadkeys "${keymap_path}"
