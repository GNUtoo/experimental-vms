#!/bin/sh
#
# Copyright (C) 2024 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This file is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# Libre en communs physical machines have Trisquel and just installed
# Guix through the package manager. So we end up with Guix 1.3.0 which
# doesn't have guix shell. Once Guix is upgraded with 'guix pull', we
# still don't have 'guix shell' without exporting the variables below.
GUIX_PROFILE="${HOME}/.config/guix/current"
. "$GUIX_PROFILE/etc/profile"

# The Libre en communs physical machines lack many packages, so when
# this is the case we can simply use Guix to get these packages, but
# at the same time resources are also very constrained (because the
# hosts typically run many virtual machines), so if some host packages
# are available, we prefer to use that as this uses way less space.
guix_packages=" \
	autoconf \
	automake \
	make \
	mtools \
	xorriso \
"
guix shell ${guix_packages} -- $@
